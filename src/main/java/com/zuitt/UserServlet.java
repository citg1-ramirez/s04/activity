package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	
	private static final long serialVersionUID = 7560556849380195433L;

	
	public void init () {
		System.out.println("*** Database connection initialized. ***");
	}
	
	
	public void doPost (HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String email = req.getParameter("emailadd");
		String contact = req.getParameter("contactnum");
		
		System.getProperties().put("firstname", firstname);
		
		HttpSession session = req.getSession();
		session.setAttribute("lastname", lastname);
		
		ServletContext scontext = getServletContext();
		scontext.setAttribute("email", email); 
		
		res.sendRedirect("details?contact="+contact);		
	}
	
	
	public void destroy () {
		System.out.println("*** Database connection destroyed. ***");
	}
}
