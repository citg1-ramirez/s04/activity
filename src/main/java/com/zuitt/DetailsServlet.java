package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	
	private static final long serialVersionUID = -7758676644158652494L;

	
	public void init() {
		System.out.println("*** Database connection initialized. ***");
	}

	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		String firstname = System.getProperty("firstname");
		
		HttpSession session = req.getSession();
		String lastname = session.getAttribute("lastname").toString();
		
		ServletContext scontext = req.getServletContext();
		String branding = scontext.getInitParameter("branding");
		String emailadd = scontext.getAttribute("email").toString();

		String contactnum = req.getParameter("contact");
		

		PrintWriter out = res.getWriter();

		out.println(
				"<h1 style=\"color:#25327D;\" >" + branding + "</h1>" + 
				"<p style=\"color:#131D55;\"> First Name: <b>" + firstname + "</b> </p>" + 
				"<p style=\"color:#131D55;\"> Last Name: <b>" + lastname + "</b> </p>" +
				"<p style=\"color:#131D55;\"> Contact Number: <b>" + contactnum + "</b> </p>" +
				"<p style=\"color:#131D55;\"> Email Address: <b>" + emailadd + "</b> </p>"
				
		);
	}
	

	public void destroy () {
		System.out.println("*** Database connection destroyed. ***");
	}
}
